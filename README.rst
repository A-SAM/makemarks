HOWTO
=====

Very simple script to compute piece-wise linear grading scales

Online help
-----------

::

   python make_marks.py --help

::

    usage: make_marks.py [-h] [-p P] [-m M] [-max MAX] [-pi PI] [-mi MI] [-i I]
                         [-t] [-o O]

    optional arguments:
      -h, --help  show this help message and exit
      -p P        A list of points (in valid python list notation and w/o spaces)
      -m M        A list of marks (in valid python list notation and w/o spaces)
      -max MAX    Maximal number of points possible
      -pi PI      Interval between points (default `0.5`)
      -mi MI      Interval between marks (default `0.25`)
      -i I        Read points and marks from this file
      -t          Transpose points/marks table read from given file
      -o O        Write points and marks to this file

Example
-------

The call

::

   python make_marks.py -p [0,21,52] -m [1,4,6] -max 72

will print the scale to ``stdout``

::

    Input specified
    ===============
    Points -> Mark:
    [[ 0  1]
     [21  4]
     [52  6]]
    Computed marks
    ==============
    Points -> Mark:
    [[  0.     1.  ]
     [  1.5    1.  ]
     [  2.     1.25]
     [  3.     1.25]
     [  3.5    1.5 ]
     [  5.     1.5 ]
     [  5.5    1.75]
     [  6.5    1.75]
     [  7.     2.  ]
     [  8.5    2.  ]
     [  9.     2.25]
     [ 10.     2.25]
     [ 10.5    2.5 ]
     [ 12.     2.5 ]
     [ 12.5    2.75]
     [ 13.5    2.75]
     [ 14.     3.  ]
     [ 15.5    3.  ]
     [ 16.     3.25]
     [ 17.     3.25]
     [ 17.5    3.5 ]
     [ 19.     3.5 ]
     [ 19.5    3.75]
     [ 20.5    3.75]
     [ 21.     4.  ]
     [ 24.5    4.  ]
     [ 25.     4.25]
     [ 28.5    4.25]
     [ 29.     4.5 ]
     [ 32.5    4.5 ]
     [ 33.     4.75]
     [ 36.     4.75]
     [ 36.5    5.  ]
     [ 40.     5.  ]
     [ 40.5    5.25]
     [ 44.     5.25]
     [ 44.5    5.5 ]
     [ 48.     5.5 ]
     [ 48.5    5.75]
     [ 51.5    5.75]
     [ 52.     6.  ]
     [ 72.     6.  ]]

and write two files: a plot ``exam.marks.png`` for visual inspection
and ``exam.marks`` which is compatible with the new exam statistics tool.
