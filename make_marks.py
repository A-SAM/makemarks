import argparse
from numpy import squeeze, arange, array, floor, unique, argwhere, hstack, zeros_like, vstack, savetxt, loadtxt, delete
from numpy.linalg import norm
from matplotlib.pyplot import figure, plot, xlim, ylim, grid, xlabel, ylabel, savefig


def make_marks(p, m, max_points=None, point_interval=0.5, mark_interval=0.25):
    """
    For each possible number of points, a mark is calculated through
    piecewise linear interpolation.

    :param p: A list of points.
    :param m: A list of marks corresponding to the points in `p`
    :param max_points: Maximal number of points possible (default `max(p)`).
    :param mark_interval: Interval between marks (default `0.25`)
    :param point_interval: Interval between points (default `0.5`)
    """
    if max_points is None:
        max_points = p.max()

    # initialize arrays of all possible points, and corresponding marks
    points = arange(0, max_points+point_interval, point_interval)
    marks = zeros_like(points)

    # Interpolate between each set of points
    for i, _ in enumerate(p[:-1]):
        I = ((points >= p[i]) & (points <= p[i+1]))
        marks[I] = (m[i+1] - m[i]) / (1.0*(p[i+1] - p[i])) * (points[I] - p[i]) + m[i]

    # Extrapolate at the beginning and the end
    I = (points <= p[0])
    marks[I] = (m[1] - m[0]) / (p[1] - p[0]) * (points[I] - p[0]) + m[0]
    I = (points >= p[-1])
    marks[I] = (m[-1] - m[-2]) / (p[-1] - p[-2]) * (points[I] - p[-1]) + m[-1]

    # Round marks to mark_interval, set maximum and minimum value
    #marks = round(marks/mark_interval)*mark_interval
    marks = floor(marks/(1.0*mark_interval)) * mark_interval
    marks[marks > 6] = 6
    marks[marks < 1] = 1

    # find the min/max number of points for each mark
    indmi = array([ argwhere(marks==ma).min() for ma in unique(marks) ])
    indma = array([ argwhere(marks==ma).max() for ma in unique(marks) ])
    ind = hstack([zeros_like(indmi), zeros_like(indma)])
    ind[0::2] = indmi
    ind[1::2] = indma

    return points, marks, ind


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-p",
                        type = str,
                        help = "A list of points (in valid python list notation and w/o spaces)")

    parser.add_argument("-m",
                        type = str,
                        help = "A list of marks (in valid python list notation and w/o spaces)")

    parser.add_argument("-max",
                        type = float,
                        help = "Maximal number of points possible",
                        default = None)

    parser.add_argument("-pi",
                        type = float,
                        help = "Interval between points (default `0.5`)",
                        default = 0.5)

    parser.add_argument("-mi",
                        type = float,
                        help = "Interval between marks (default `0.25`)",
                        default = 0.25)

    parser.add_argument("-i",
                        type = str,
                        help = "Read points and marks from this file")

    parser.add_argument("-t",
                        action = "store_true",
                        help = "Transpose points/marks table read from given file")

    parser.add_argument("-o",
                        type = str,
                        help = "Write points and marks to this file",
                        default = "exam.marks")

    args = parser.parse_args()

    # Check input values
    if args.p is not None and args.m is not None:
        p = squeeze(array(eval(args.p)))
        m = squeeze(array(eval(args.m)))
        data = vstack([p, m]).T
    elif args.i is not None:
        data = loadtxt(args.i)
        if args.t:
            data = data.T
    else:
        raise IOError("No points/marks data given. Call 'make_marks.py -h' for help.")

    # Remove duplicates
    no = norm(data[:-1,:] - data[1:,:], axis=1)
    remove = [ row for row in xrange(1, data.shape[0]) if no[row-1] <= 1e-10 ]
    data = delete(data, remove, axis=0)
    p = squeeze(data[:,0])
    m = squeeze(data[:,1])

    if not p.shape == m.shape or p.shape[0] < 2:
        raise ValueError("Bad Input: p and m must be lists of equal length >= 2")

    print("Input specified")
    print("===============")
    print("Points -> Mark:")
    print(vstack([p, m]).T)

    points, marks, ind = make_marks(p, m, args.max, args.pi, args.mi)
    gradingscale = vstack([points[ind], marks[ind]]).T

    print("Computed marks")
    print("==============")
    print("Points -> Mark:")
    print(gradingscale)

    # Plot the obtained marks
    figure()
    plot(points, marks, 'x-')
    plot(p, m, 'ro')
    plot(p, m, 'r--')
    xlim(0, points.max())
    ylim(1, 6)
    grid(True)
    xlabel("Points")
    ylabel("Marks")
    savefig(args.o+".png")

    # Write to file
    f = open(args.o, "w")
    savetxt(f, gradingscale, fmt=("%.1f", "%.2f"))
    f.close()
